"""
 # @ Author: Loueï Bekhedda
 # @ Create Time: 2024-03-12 15:42:06
 # @ Modified by: Loueï Bekhedda
 # @ Modified time: 2024-03-25 01:06:46
 # @ Description: Permet de stocker les informations afin de rendre la compilation possible 
 # (par ex : noms, type, adresse des identificateurs)
 """

# Importation de la classe qui représente les Identificateurs
from analex import Identifier
from typing import Dict, Optional, Any
from pprint import pprint


class IdentifierType:
    """
    Cette classe représente les différents types des identificateurs.
    """

    # **VARIABLE
    INTEGER = "integer"
    BOOLEAN = "boolean"
    # **CALLABLE
    FUNCTION = "function"
    PROCEDURE = "procedure"


class IdentifierInfo:
    def __init__(
        self,
        id_type: IdentifierType,
        scope: str,
        value=None,
        address: int = None,
        is_in: bool = None,
        is_in_out: bool = None,
    ):
        """
        Constructeur pour les informations d'un identificateur.

        Args:
            id_type (IdentifierType): Le type de l'identificateur (par exemple, "integer", "boolean", "function", "procedure").
            scope (str): La portée de l'identificateur (par exemple, "global", "local", "parameter").
            value: La valeur initiale de l'identificateur, si applicable. Sa valeur par défaut est None.
            address (int): L'adresse mémoire de l'identificateur, si applicable. Sa valeur par défaut est None.

        """
        self.id_type = id_type
        self.scope = scope
        self.value = value
        self.address = address
        self.is_in = is_in
        self.is_in_out = is_in_out

    def __repr__(self):
        return f"<IdentifierInfo type={self.id_type}, scope={self.scope}, value={self.value}, in={self.is_in}, in_out={self.is_in_out}>"

    def is_callable(self) -> bool:
        """Vérifie si l'identificateur spécifié est un appelable."""
        return self.id_type in [IdentifierType.FUNCTION, IdentifierType.PROCEDURE]

    def is_variable(self) -> bool:
        """Vérifie si l'identificateur spécifié est une variable."""
        return self.id_type in [IdentifierType.INTEGER, IdentifierType.BOOLEAN]

    def get_type(self) -> str:
        """Renvoie le type de l'identificateur."""
        return self.id_type

    def get_scope(self) -> str:
        """Renvoie la portée de l'identificateur."""
        return self.scope

    def get_value(self) -> any:
        """Renvoie la valeur de l'identificateur."""
        return self.value

    def get_address(self) -> int:
        """Renvoie l'adresse mémoire de l'identificateur."""
        return self.address

    def set_address(self, address: int) -> None:
        """Définit l'adresse mémoire de l'identificateur."""
        self.address = address

    def set_value(self, value: any) -> None:
        """Définit la valeur de l'identificateur."""
        self.value = value

    def set_type(self, id_type: IdentifierType) -> None:
        """Définit le type de l'identificateur."""
        self.id_type = id_type

    def set_scope(self, scope: str) -> None:
        """Définit la portée de l'identificateur."""
        self.scope = scope

    def is_integer(self) -> bool:
        """Vérifie si l'identificateur est de type entier."""
        return self.id_type == IdentifierType.INTEGER

    def is_boolean(self) -> bool:
        """Vérifie si l'identificateur est de type booléen."""
        return self.id_type == IdentifierType.BOOLEAN

    def is_function(self) -> bool:
        """Vérifie si l'identificateur est une fonction."""
        return self.id_type == IdentifierType.FUNCTION

    def is_procedure(self) -> bool:
        """Vérifie si l'identificateur est une procédure."""
        return self.id_type == IdentifierType.PROCEDURE

    def is_in_status(self) -> bool:
        """Vérifie si l'identificateur est en mode IN."""
        return self.is_in

    def is_in_out_status(self) -> bool:
        """Vérifie si l'identificateur est en mode IN OUT."""
        return self.is_in_out


class IdentifierTable:
    """
    Cette classe représente une table des identificateurs.

    Une table des identificateurs est utilisée pour stocker et gérer les identificateurs
    utilisés dans un programme. Elle permet d'ajouter, supprimer et rechercher des identificateurs
    dans la table.
    """

    def __init__(self):
        self.table: Dict[str, IdentifierInfo] = {}

    def add_identifier(
        self,
        name: str,
        id_type: IdentifierType,
        scope: str,
        value: Optional[Any] = None,
        address: Optional[int] = None,
        is_in: Optional[bool] = None,
        is_in_out: Optional[bool] = None,
    ) -> None:
        """
        Ajoute un nouvel identificateur à la table avec des informations bien définies.

        Args:
            name (str): Le nom de l'identificateur.
            id_type (IdentifierType): Le type de l'identificateur.
            scope (str): La portée de l'identificateur.
            value (Optional[any]): La valeur initiale de l'identificateur, si applicable.
        """
        if name in self.table:
            raise Exception(f"L'identificateur '{name}' existe déjà dans la table.")
        else:
            self.table[name] = IdentifierInfo(
                id_type, scope, value, address, is_in, is_in_out
            )

    def remove_identifier(self, name: str) -> None:
        """
        Supprime un identificateur de la table.

        Args:
            name (str): Le nom de l'identificateur à supprimer.
        """
        if name in self.table:
            del self.table[name]
        else:
            raise Exception(f"L'identificateur '{name}' n'existe pas dans la table.")

    def find_identifier(self, name: str) -> Optional[IdentifierInfo]:
        """
        Recherche un identificateur dans la table et renvoie ses informations.

        Args:
            name (str): Le nom de l'identificateur recherché.

        Returns:
            dict: Les informations de l'identificateur trouvé.
        """

        return self.table.get(name, None)

    def get_name_and_adresse_procedure(self):
        """Récupération de toute les adresses et de leur nom"""
        tab_proc = (
            []
        )  # tableau contenant toute les nom de fonction/procédure et leur adresse
        for (
            name,
            identifier,
        ) in (
            self.table.items()
        ):  # boucle pour le parcourt la recherche de procédure et de fonction
            if (
                identifier.get_type() == "procedure"
                or identifier.get_type() == "function"
            ):  # si l'identificateur est de type procédure ou fonction
                tab_proc.append(
                    [name, identifier.get_address]
                )  # remplir le tableau des procédure/fonction avec le nom et l'adresse
        return tab_proc  # retourner le tableau des fonction

    def find_identifier_by_address(
        self, address: int, scope_id: int
    ) -> Optional[IdentifierInfo]:
        """
        Recherche un identificateur dans la table par son adresse et renvoie ses informations.
        Args:
            address (int): L'adresse de l'identificateur recherché.
            scope_id (int): Le numéro du bloc recherché.
        Returns:
            IdentifierInfo: Les informations de l'identificateur trouvé ou None s'il n'est pas trouvé.
        """
        tab_proc = (
            self.get_name_and_adresse_procedure()
        )  # récupération de toute les adresses et nom de toute les fonctions/procédure
        name_proc = tab_proc[
            scope_id
        ]  # récupération du nom correspondant au nom du bloc
        for (
            identifier
        ) in self.table.values():  # boucle de parcourt de la table des identificateur
            if identifier.get_address() == address and (
                identifier.get_type() == "function"
                or identifier.get_type() == "function"
            ):  # si l'adresse dynamique et que le type correspondant ( cas de procédure ou de fonction)
                return identifier  # retourner l'identificateur
            elif identifier.get_address() == address and (
                (
                    identifier.get_type() == "integer"
                    or identifier.get_type() == "boolean"
                )
                and identifier.get_scope() == name_proc[0]
            ):  # si l'adresse, que la porté et le type correspondent
                return identifier  # retourner l'identificateur
        return None  # si rien n'a été trouvé alors retoruner None

    def update_identifier(self, name: str, kwargs) -> Dict:
        """
        Met à jour les informations d'un identificateur existant dans la table.
        Args:
            name (str): Le nom de l'identificateur à mettre à jour.
            kwatgs (IdentifierInfo): Les nouvelles informations de l'identificateur.
        """
        if name in self.table:  # parcourt de la table
            self.table[name].set_value(
                kwargs.get_value()
            )  # modifier l'identificateur en fonction du nom
        else:
            raise Exception(f"L'identificateur '{name}' n'existe pas dans la table.")

    def get_identifier_name_by_id(self, id, numberBloc, dynamiqueAdd):
        """Récupération du nom de l'identificateur en fonction de l'identificateur, du bloc et de son adresse dynamique"""
        i = 0  # indice dans la table de l'identificateur
        tab_proc = (
            self.get_name_and_adresse_procedure()
        )  # tableau de toute les procédure/fonction
        nameProc = []  # nom de toute les fonction/procédure
        for name in tab_proc:  # boucle pour remplir pour
            nameProc.append(name[0])  # remplir le tableau de nom de fonction/procédure
        if numberBloc > len(nameProc) - 1 and (
            id.get_scope() in nameProc
        ):  # si le numéro de bloc est supérieur au bloc alors on est dans le cas récursif
            for (
                y
            ) in (
                name
            ):  # boucle pour comparer la porté de l'identificateur avec le nom de la procédure
                if (
                    id.get_scope() == y
                ):  # si le scope de l'identificateur correspond au nom de la fonction
                    numberBloc = i  # l'indice dans le tableau devient le numéro de bloc
                i += 1  # incrémenter pour la recherhce dans le tableau
        idlist = []  # liste des identificateur qui ont la meme porté
        for (
            name,
            info,
        ) in self.table.items():  # parcourt de la table des identificateurs
            if (
                info.get_scope() == nameProc[numberBloc]
            ):  # si la porté est bonne et que cela correspond au nom de bloc
                idlist.append(
                    name
                )  # ajout de l'identificateur qui est une variable local à la fonction
        if (
            len(idlist) == 0
        ):  # si la liste des identificateur est vide alors on a rien trouvé
            return None
        else:
            return idlist[
                dynamiqueAdd
            ]  # renvoyé l'identificateur à l'adresse dynamique correspondante

    def display_identifiers(self) -> Dict:
        """
        Affiche le contenu de la table des identificateurs.
        """
        for name, info in self.table.items():
            pprint(f"{name}: {info}")
