"""
 # @ Author: Damien Lolive
 # @ Create Time: N/A
 # @ Modified by: Loueï Bekhedda
 # @ Modified time: 2024-04-29 20:01:00
 # @ Description: Syntactical Analyser package.
 """

#!/usr/bin/python

## 	@package anasyn
# 	Syntactical Analyser package.
#

import sys, argparse, re
import logging

import analex

# * Import de la table des identificateurs
from tableid import IdentifierTable, IdentifierInfo, IdentifierType

# * Import du compilateur
from compil import Compiler
from pprint import pprint
from VM import VirtualMachine

logger = logging.getLogger("anasyn")

DEBUG = False
LOGGING_LEVEL = logging.DEBUG

# * Instanciation du compilateur
compiler = Compiler()
used_var = False
type_sommet_pile = []
operation = []
class AnaSynException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


########################################################################
#### Syntactical Diagrams
########################################################################


def program(lexical_analyser):
    # * Point de génération du code
    # Ajout de l'instruction debutProg
    compiler.debut_prog()
    specifProgPrinc(lexical_analyser)
    lexical_analyser.acceptKeyword("is")
    corpsProgPrinc(lexical_analyser)


def specifProgPrinc(lexical_analyser):
    lexical_analyser.acceptKeyword("procedure")
    ident = lexical_analyser.acceptIdentifier()
    # ? Les identificateurs sont stockés dans lexical_analyser.lexical_units
    # ? On peut donc les récupérer en utilisant lexical_analyser.lexical_units[index]
    # ? Ici l'index doit être décalé de 1 car on a déjà accepté le mot-clé "procedure"
    # ? On peut donc récupérer le nom du programme en utilisant lexical_analyser.lexical_units[lexical_analyser.lexical_unit_index - 1]
    accepted_ident = lexical_analyser.lexical_units[
        lexical_analyser.lexical_unit_index - 1
    ]

    # * Structure d'ident
    # * l: ligne index
    # * c: colonne index
    # * ln: longueur
    # * v: valeur

    # * Point de génération du code
    # Ajout d'un identificateur de type PROCEDURE
    """
    Compte tenu qu’il n’est jamais nécessaire de désigner l’entité programme
    principal (l’appel de cette procédure est interdite dans le langage), on considère
    que la portée de l’identificateur de la procédure principale est vide.
    """
    compiler.add_identifier(
        name=accepted_ident.value,
        id_type=IdentifierType.PROCEDURE,
        scope=None,
        value=None,
        address=-1,
    )

    compiler.prog_princ = accepted_ident.value

    logger.debug("Name of program : " + ident)


def corpsProgPrinc(lexical_analyser):
    if not lexical_analyser.isKeyword("begin"):
        logger.debug("Parsing declarations")
        partieDecla(lexical_analyser)
        logger.debug("End of declarations")

        # * Point de génération du code
        if compiler.identifier_counter > 0:
            # ? Si il y a des identificateurs non réservés
            compiler.reserver(compiler.identifier_counter)
            compiler.identifier_counter = 0

    lexical_analyser.acceptKeyword("begin")

    if not lexical_analyser.isKeyword("end"):
        logger.debug("Parsing instructions")
        suiteInstr(lexical_analyser)
        logger.debug("End of instructions")

    lexical_analyser.acceptKeyword("end")
    lexical_analyser.acceptFel()
    # * Point de génération du code
    # Ajout de l'instruction finProg
    compiler.fin_prog()
    logger.debug("End of program")


def partieDecla(lexical_analyser):
    if lexical_analyser.isKeyword("procedure") or lexical_analyser.isKeyword(
        "function"
    ):
        # ? On indique qu'on a trouvé une procédure/fonction
        compiler.callable_counter += 1

        # ? Position à laquelle on va insérer le tra
        pos = len(compiler.instructions)

        listeDeclaOp(lexical_analyser)

        # * Point de génération du code
        tra_address = len(compiler.instructions) + 1 + 1
        # * Point de génération du code
        compiler.put_instruction(pos, ["tra", tra_address])
        if not lexical_analyser.isKeyword("begin"):
            listeDeclaVar(lexical_analyser)

    else:
        listeDeclaVar(lexical_analyser)


def listeDeclaOp(lexical_analyser):
    # ? Fonction qui regarde si il y a des procedures ou fonctions
    declaOp(lexical_analyser)
    lexical_analyser.acceptCharacter(";")
    if lexical_analyser.isKeyword("procedure") or lexical_analyser.isKeyword(
        "function"
    ):
        listeDeclaOp(lexical_analyser)


def declaOp(lexical_analyser):
    if lexical_analyser.isKeyword("procedure"):
        procedure(lexical_analyser)
    if lexical_analyser.isKeyword("function"):
        fonction(lexical_analyser)


def procedure(lexical_analyser):
    lexical_analyser.acceptKeyword("procedure")
    ident = lexical_analyser.acceptIdentifier()
    logger.debug("Name of procedure : " + ident)
    # * Point de génération du code
    # Ajout d'un identificateur de type PROCEDURE
    accepted_ident = lexical_analyser.lexical_units[
        lexical_analyser.lexical_unit_index - 1
    ]
    compiler.add_identifier(
        accepted_ident.value,
        IdentifierType.PROCEDURE,
        scope=None,
        value=None,
        address=None,
    )
    partieFormelle(lexical_analyser)

    # ? On réinitialise l'adresse courante
    # compiler.reset_address()

    lexical_analyser.acceptKeyword("is")
    corpsProc(lexical_analyser)
    # * Point de génération du code
    compiler.retour_proc()


def fonction(lexical_analyser):
    lexical_analyser.acceptKeyword("function")
    ident = lexical_analyser.acceptIdentifier()
    logger.debug("Name of function : " + ident)
    # * Point de génération du code
    # Ajout d'un identificateur de type FUNCTION
    accepted_ident = lexical_analyser.lexical_units[
        lexical_analyser.lexical_unit_index - 1
    ]
    compiler.add_identifier(
        name=accepted_ident.value,
        id_type=IdentifierType.FUNCTION,
        scope=compiler.scope,
        value=None,
        address=None,
    )

    partieFormelle(lexical_analyser)

    lexical_analyser.acceptKeyword("return")
    type = nnpType(lexical_analyser)
    compiler.identifier_table.table[accepted_ident.value].set_type(type)
    lexical_analyser.acceptKeyword("is")
    corpsFonct(lexical_analyser)


def corpsProc(lexical_analyser):
    if not lexical_analyser.isKeyword("begin"):
        partieDeclaProc(lexical_analyser)

    # * Point de génération du code
    if compiler.identifier_counter > 0:
        # ? Si il y a des identificateurs non réservés
        compiler.reserver(compiler.identifier_counter)
        compiler.identifier_counter = 0

    lexical_analyser.acceptKeyword("begin")
    compiler.reset_address()
    suiteInstr(lexical_analyser)
    lexical_analyser.acceptKeyword("end")
    # * Point de génération du code
    # ? Permet de sortir de la portée actuelle
    # ? pour revenir à la portée précédente
    compiler.leave_scope()


def corpsFonct(lexical_analyser):
    if not lexical_analyser.isKeyword("begin"):
        partieDeclaProc(lexical_analyser)

    # * Point de génération du code
    if compiler.identifier_counter > 0:
        # ? Si il y a des identificateurs non réservés
        compiler.reserver(compiler.identifier_counter)
        compiler.identifier_counter = 0

    lexical_analyser.acceptKeyword("begin")
    compiler.leave_scope()
    suiteInstrNonVide(lexical_analyser)
    lexical_analyser.acceptKeyword("end")
    # * Point de génération du code
    # ? Permet de sortir de la portée actuelle
    # ? pour revenir à la portée précédente
    compiler.reset_address()


def partieFormelle(lexical_analyser):
    lexical_analyser.acceptCharacter("(")
    if not lexical_analyser.isCharacter(")"):
        listeSpecifFormelles(lexical_analyser)
    lexical_analyser.acceptCharacter(")")


def listeSpecifFormelles(lexical_analyser):
    specif(lexical_analyser)
    if not lexical_analyser.isCharacter(")"):
        lexical_analyser.acceptCharacter(";")
        listeSpecifFormelles(lexical_analyser)


def specif(lexical_analyser):
    listeIdent(lexical_analyser)
    lexical_analyser.acceptCharacter(":")
    is_in_out = False
    is_in = False
    if lexical_analyser.isKeyword("in"):
        # * On sait que c'est un paramètre in
        is_in = True
        # * Booléen pour savoir si c'est un paramètre in out
        is_in_out = mode(lexical_analyser)

    type = nnpType(lexical_analyser)

    # * Point de génération du code
    while compiler.identifier_queue:
        accepted_ident = compiler.identifier_queue.pop(0)

        compiler.add_identifier(
            name=accepted_ident.value,
            id_type=type,
            scope=compiler.scope,
            value=None,
            address=compiler.current_address,
            is_in=is_in,
            is_in_out=is_in_out,
        )
        # ? On incrémente l'adresse courante
        compiler.increment_address()


def mode(lexical_analyser):
    lexical_analyser.acceptKeyword("in")
    # * Booléen pour savoir si c'est un paramètre in out
    is_in_out = False
    if lexical_analyser.isKeyword("out"):
        lexical_analyser.acceptKeyword("out")
        # * Booléen pour savoir si c'est un paramètre in out
        is_in_out = True
        logger.debug("in out parameter")
    else:
        logger.debug("in parameter")
    return is_in_out


def nnpType(lexical_analyser):
    type = None
    if lexical_analyser.isKeyword("integer"):
        lexical_analyser.acceptKeyword("integer")
        logger.debug("integer type")
        type = IdentifierType.INTEGER
    elif lexical_analyser.isKeyword("boolean"):
        lexical_analyser.acceptKeyword("boolean")
        logger.debug("boolean type")
        type = IdentifierType.BOOLEAN
    else:
        logger.error("Unknown type found <" + lexical_analyser.get_value() + ">!")
        raise AnaSynException(
            "Unknown type found <" + lexical_analyser.get_value() + ">!"
        )
    return type


def partieDeclaProc(lexical_analyser):
    listeDeclaVar(lexical_analyser)
    # ? On réinitialise l'adresse courante
    # compiler.reset_address()


def listeDeclaVar(lexical_analyser):
    declaVar(lexical_analyser)

    if lexical_analyser.isIdentifier():
        listeDeclaVar(lexical_analyser)


def declaVar(lexical_analyser):
    # Nombre de blocs
    listeIdent(lexical_analyser)
    lexical_analyser.acceptCharacter(":")
    logger.debug("now parsing type...")

    type = nnpType(lexical_analyser)

    while compiler.identifier_queue:
        accepted_ident = compiler.identifier_queue.pop(0)
        compiler.identifier_counter += 1
        compiler.add_identifier(
            name=accepted_ident.value,
            id_type=type,
            scope=compiler.scope,
            value=None,
            address=compiler.current_address,
        )

        # ? On incrémente l'adresse courante
        compiler.increment_address()
    lexical_analyser.acceptCharacter(";")


def listeIdent(lexical_analyser):
    ident = lexical_analyser.acceptIdentifier()
    logger.debug("identifier found: " + str(ident))
    accepted_ident = lexical_analyser.lexical_units[
        lexical_analyser.lexical_unit_index - 1
    ]

    # * Point de génération du code
    compiler.identifier_queue.append(accepted_ident)
    if lexical_analyser.isCharacter(","):
        lexical_analyser.acceptCharacter(",")
        listeIdent(lexical_analyser)


def suiteInstrNonVide(lexical_analyser):
    instr(lexical_analyser)
    if lexical_analyser.isCharacter(";"):
        lexical_analyser.acceptCharacter(";")
        suiteInstrNonVide(lexical_analyser)


def suiteInstr(lexical_analyser):
    if not lexical_analyser.isKeyword("end"):
        suiteInstrNonVide(lexical_analyser)


def instr(lexical_analyser):
    global type_sommet_pile
    if lexical_analyser.isKeyword("while"):
        boucle(lexical_analyser)
    elif lexical_analyser.isKeyword("if"):
        altern(lexical_analyser)
    elif lexical_analyser.isKeyword("get") or lexical_analyser.isKeyword("put"):
        es(lexical_analyser)
    elif lexical_analyser.isKeyword("return"):
        retour(lexical_analyser)
    elif lexical_analyser.isIdentifier():
        ident = lexical_analyser.acceptIdentifier()
        logger.debug("identifier found: " + str(ident))
        # * Recherche de l'identificateur précedemment déclaré
        searched_ident = compiler.identifier_table.find_identifier(str(ident))

        # ? On empile l'adresse de l'identificateur
        # compiler.empiler(searched_ident.get_address(), 1)

        if lexical_analyser.isSymbol(":="):
            # ? Cas d'une affectation de valeur
            # ! TODO: Gestion des erreurs

            # Affectation
            lexical_analyser.acceptSymbol(":=")
            # { a := <e> }
            # Traduction de <e>

            # ? On regarde si la variable est locale
            if searched_ident.scope != compiler.prog_princ:
                # ? Si l'identificateur est une variable in
                if searched_ident.is_in_status():
                    # ? Si l'identificateur est une variable in out
                    if searched_ident.is_in_out_status():
                        # * Point de génération du code
                        compiler.empiler_param(searched_ident.get_address())
                        used_var = True
                    else:
                        # ! Si l'identificateur est un paramètre in uniquement
                        # ! TODO: Gestion de ce cas
                        raise AnaSynException(
                            f"Trying to assign a value to a read-only variable! <{ident}>"
                        )
                else:
                    # * Point de génération du code
                    compiler.empiler_ad(searched_ident.get_address())
                    used_var = True
            else:
                compiler.empiler(searched_ident.get_address(), 1)
                used_var = True

            expression(lexical_analyser)
            if len(type_sommet_pile)>1:
                type_sommet_pile.pop()
            if type_sommet_pile[-1] == searched_ident.get_type():
                compiler.affectation()
                #type_sommet_pile.pop()
                
                logger.debug("parsed affectation")
            else:
                raise AnaSynException(
                            f"La variable doit être du meme type que la valeur d'affectation!"
                        )

        elif lexical_analyser.isCharacter("("):
            # * Point de génération du code
            # compiler.
            lexical_analyser.acceptCharacter("(")
            # ! TODO pour NNP
            # * Point de génération du code
            compiler.reserver_bloc()
            compiler.param_counter = 0
            if not lexical_analyser.isCharacter(")"):
                listePe(lexical_analyser)
            lexical_analyser.acceptCharacter(")")
            # * Point de génération du code
            compiler.tra_stat(searched_ident.get_address() + 1, compiler.param_counter)
            logger.debug("parsed procedure call")
        else:
            logger.error("Expecting procedure call or affectation!")
            raise AnaSynException("Expecting procedure call or affectation!")

    else:
        logger.error("Unknown Instruction <" + lexical_analyser.get_value() + ">!")
        raise AnaSynException(
            "Unknown Instruction <" + lexical_analyser.get_value() + ">!"
        )


def listePe(lexical_analyser):
    expression(lexical_analyser)
    if lexical_analyser.isCharacter(","):
        lexical_analyser.acceptCharacter(",")
        listePe(lexical_analyser)
    compiler.param_counter += 1


def expression(lexical_analyser):
    logger.debug("parsing expression: " + str(lexical_analyser.get_value()))

    exp1(lexical_analyser)

    if lexical_analyser.isKeyword("or"):
        #type_sommet_pile.pop()
        type_sommet_pile.append("boolean")
        lexical_analyser.acceptKeyword("or")
        exp1(lexical_analyser)
        # * Point de génération du code
        compiler.ou()


def exp1(lexical_analyser):
    global type_sommet_pile
    logger.debug("parsing exp1")

    exp2(lexical_analyser)
    if lexical_analyser.isKeyword("and"):
        #type_sommet_pile.pop()
        type_sommet_pile.append("boolean")
        lexical_analyser.acceptKeyword("and")
        exp2(lexical_analyser)
        # * Point de génération du code
        compiler.et()


def exp2(lexical_analyser):
    logger.debug("parsing exp2")
    operateur = None
    exp3(lexical_analyser)
    if (
        lexical_analyser.isSymbol("<")
        or lexical_analyser.isSymbol("<=")
        or lexical_analyser.isSymbol(">")
        or lexical_analyser.isSymbol(">=")
    ):
        # ? Expression avec un opérateur booléen binaire
        # ? Nécessité de stocker la méthode avant de l'appeler
        # ? Car on doit d'abord traduire E1 puis E2
        # ? {< E1 > op < E2 >} =
        # ? {< E1 >};
        # ? {< E2 >};
        # ? op()
        operateur = opRel(lexical_analyser)
        exp3(lexical_analyser)
    elif lexical_analyser.isSymbol("=") or lexical_analyser.isSymbol("/="):
        operateur = opRel(lexical_analyser)
        exp3(lexical_analyser)
    # * Après avoir tout traduit, on peut executer la méthode
    # * qui permettra de générer le code
    if operateur:
        operateur()


def opRel(lexical_analyser):
    global type_sommet_pile
    global operation
    logger.debug("parsing relationnal operator: " + lexical_analyser.get_value())

    if lexical_analyser.isSymbol("<"):
        lexical_analyser.acceptSymbol("<")
        #type_sommet_pile.pop()
        type_sommet_pile.pop()
        type_sommet_pile.append("boolean")
        operation.append(True)
        # * Point de génération du code
        return compiler.inf
    elif lexical_analyser.isSymbol("<="):
        lexical_analyser.acceptSymbol("<=")
        type_sommet_pile.append("boolean")
        operation.append(True)
        # * Point de génération du code
        return compiler.infeg

    elif lexical_analyser.isSymbol(">"):
        lexical_analyser.acceptSymbol(">")
        #type_sommet_pile.pop()
        type_sommet_pile.pop()
        type_sommet_pile.append("boolean")
        operation.append(True)
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.sup

    elif lexical_analyser.isSymbol(">="):
        lexical_analyser.acceptSymbol(">=")
        #type_sommet_pile.pop()
        type_sommet_pile.pop()
        type_sommet_pile.append("boolean")
        operation.append(True)
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.supeg

    elif lexical_analyser.isSymbol("="):
        lexical_analyser.acceptSymbol("=")
        #type_sommet_pile.pop()
        type_sommet_pile.pop()
        type_sommet_pile.append("boolean")
        operation.append(True)
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.egal

    elif lexical_analyser.isSymbol("/="):
        lexical_analyser.acceptSymbol("/=")
        #type_sommet_pile.pop()
        type_sommet_pile.pop()
        type_sommet_pile.append("boolean")
        operation.append(True)
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.diff

    else:
        msg = "Unknown relationnal operator <" + lexical_analyser.get_value() + ">!"
        logger.error(msg)
        raise AnaSynException(msg)


def exp3(lexical_analyser):
    logger.debug("parsing exp3")
    operateur = None
    exp4(lexical_analyser)
    if lexical_analyser.isCharacter("+") or lexical_analyser.isCharacter("-"):
        operateur = opAdd(lexical_analyser)
        exp4(lexical_analyser)

    if operateur:
        operateur()


def opAdd(lexical_analyser):
    global type_sommet_pile
    logger.debug("parsing additive operator: " + lexical_analyser.get_value())
    if lexical_analyser.isCharacter("+"):
        lexical_analyser.acceptCharacter("+")
        #type_sommet_pile.pop()
        type_sommet_pile.pop()
        type_sommet_pile.append("integer")
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.add

    elif lexical_analyser.isCharacter("-"):
        lexical_analyser.acceptCharacter("-")
        #type_sommet_pile.pop()
        type_sommet_pile.pop()
        type_sommet_pile.append("integer")
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.sous

    else:
        msg = "Unknown additive operator <" + lexical_analyser.get_value() + ">!"
        logger.error(msg)
        raise AnaSynException(msg)


def exp4(lexical_analyser):
    logger.debug("parsing exp4")
    operateur = None
    prim(lexical_analyser)
    if lexical_analyser.isCharacter("*") or lexical_analyser.isCharacter("/"):
        operateur = opMult(lexical_analyser)
        prim(lexical_analyser)
        if operateur:
            operateur()


def opMult(lexical_analyser):
    global type_sommet_pile
    logger.debug("parsing multiplicative operator: " + lexical_analyser.get_value())
    if lexical_analyser.isCharacter("*"):
        lexical_analyser.acceptCharacter("*")
        type_sommet_pile.pop()
        type_sommet_pile.append("integer")
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.mult

    elif lexical_analyser.isCharacter("/"):
        lexical_analyser.acceptCharacter("/")
        type_sommet_pile.pop()
        type_sommet_pile.append("integer")
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.div

    else:
        msg = "Unknown multiplicative operator <" + lexical_analyser.get_value() + ">!"
        logger.error(msg)
        raise AnaSynException(msg)


def prim(lexical_analyser):
    logger.debug("parsing prim")
    operateur = None
    if (
        lexical_analyser.isCharacter("+")
        or lexical_analyser.isCharacter("-")
        or lexical_analyser.isKeyword("not")
    ):
        # ? Expression avec l’opérateur moins unaire
        operateur = opUnaire(lexical_analyser)
    elemPrim(lexical_analyser)
    if operateur:
        operateur()


def opUnaire(lexical_analyser):
    logger.debug("parsing unary operator: " + lexical_analyser.get_value())
    if lexical_analyser.isCharacter("+"):
        lexical_analyser.acceptCharacter("+")
        # * On renvoie None si +
        return None
    elif lexical_analyser.isCharacter("-"):
        lexical_analyser.acceptCharacter("-")
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.moins
    elif lexical_analyser.isKeyword("not"):
        lexical_analyser.acceptKeyword("not")
        # * Point de génération du code
        # ? On retourne la méthode sans l'appeler
        # ? pour pouvoir l'appeler après avoir traduit
        return compiler.non
    else:
        msg = "Unknown additive operator <" + lexical_analyser.get_value() + ">!"
        logger.error(msg)
        raise AnaSynException(msg)


def elemPrim(lexical_analyser):
    logger.debug("parsing elemPrim: " + str(lexical_analyser.get_value()))
    global used_var
    global type_sommet_pile
    if lexical_analyser.isCharacter("("):
        lexical_analyser.acceptCharacter("(")
        expression(lexical_analyser)
        lexical_analyser.acceptCharacter(")")
    elif (
        lexical_analyser.isInteger()
        or lexical_analyser.isKeyword("true")
        or lexical_analyser.isKeyword("false")
    ):
        valeur(lexical_analyser)
    elif lexical_analyser.isIdentifier():
        ident = lexical_analyser.acceptIdentifier()
        searched_ident = compiler.identifier_table.find_identifier(ident)
        if lexical_analyser.isCharacter("("):  # Appel fonct
            # * Point de génération du code
            # ! Ajouter suite
            lexical_analyser.acceptCharacter("(")
            # * Point de génération du code
            # ? Permet de réserver un bloc pour les variables locales
            compiler.reserver_bloc()
            compiler.param_counter = 0
            if not lexical_analyser.isCharacter(")"):
                listePe(lexical_analyser)
            lexical_analyser.acceptCharacter(")")
            # * Point de génération du code
            logger.debug("parsed procedure call")
            logger.debug("Call to function: " + ident)
            # * Point de génération du code
            compiler.tra_stat(searched_ident.get_address() + 1, compiler.param_counter)
        else:
            logger.debug("Use of an identifier as an expression: " + ident)
            if searched_ident.scope != compiler.prog_princ:
                # ? Si l'identificateur est une variable in
                if searched_ident.is_in_status():
                    # ? Si l'identificateur est une variable in out
                    if searched_ident.is_in_out_status():
                        # * Point de génération du code
                        compiler.empiler_param(searched_ident.get_address())
                    else:
                        # ! Si l'identificateur est un paramètre in uniquement
                        compiler.empiler_ad(searched_ident.get_address())
                        used_var = True
                    compiler.valeur_pile()
                    if searched_ident.is_boolean():
                        type_sommet_pile.append("boolean")
                    else:
                        type_sommet_pile.append("integer")
                else:
                    compiler.empiler_ad(searched_ident.get_address())
                    used_var = True
                    # ? Expressions réduites à une variable
                    # * Point de génération du code
                    # compiler.empiler(searched_ident.get_address(), 1)
                    compiler.valeur_pile()
                    if searched_ident.is_boolean():
                        type_sommet_pile.append("boolean")
                    else:
                        type_sommet_pile.append("integer")
                    
            else:
                compiler.empiler(searched_ident.get_address(), 1)
                compiler.valeur_pile()
                if searched_ident.is_boolean():
                    type_sommet_pile.append("boolean")
                else:
                    type_sommet_pile.append("integer")
    else:
        logger.error("Unknown Value!")
        raise AnaSynException("Unknown Value!")


def valeur(lexical_analyser):
    global used_var
    global type_sommet_pile
    if lexical_analyser.isInteger():
        entier = lexical_analyser.acceptInteger()
        logger.debug("integer value: " + str(entier))
        # * Point de génération du code
        # ? On empile la valeur de l'entier
        compiler.empiler(entier, 0)
        type_sommet_pile.append("integer")
        used_var = True
        return entier
    elif lexical_analyser.isKeyword("true") or lexical_analyser.isKeyword("false"):
        booleen = valBool(lexical_analyser)
        return booleen
    else:
        logger.error("Unknown Value! Expecting an integer or a boolean value!")
        raise AnaSynException(
            "Unknown Value ! Expecting an integer or a boolean value!"
        )


def valBool(lexical_analyser):
    global type_sommet_pile
    if lexical_analyser.isKeyword("true"):
        lexical_analyser.acceptKeyword("true")
        # * Point de génération du code
        compiler.empiler(1, 3)
        type_sommet_pile.append("boolean")
        logger.debug("boolean true value")
        return True
    else:
        logger.debug("boolean false value")
        lexical_analyser.acceptKeyword("false")
        # * Point de génération du code
        type_sommet_pile.append("boolean")
        compiler.empiler(0, 3)
        return False

def es(lexical_analyser):
    logger.debug("parsing E/S instruction: " + lexical_analyser.get_value())
    global used_var
    global type_sommet_pile
    if lexical_analyser.isKeyword("get"):
        lexical_analyser.acceptKeyword("get")
        lexical_analyser.acceptCharacter("(")
        ident = lexical_analyser.acceptIdentifier()
        lexical_analyser.acceptCharacter(")")
        logger.debug("Call to get " + ident)
        # * Point de génération du code
        # ? On recherche l'identificateur dans la table des identificateurs
        searched_ident = compiler.identifier_table.find_identifier(ident)
        compiler.empiler(searched_ident.get_address(), 1)
        used_var = True
        if searched_ident.is_boolean():
            logger.error("Variable booléenne incompatible avec get")
            raise AnaSynException("Variable booléenne incompatible avec get!")
        compiler.get()
    elif lexical_analyser.isKeyword("put"):
        lexical_analyser.acceptKeyword("put")
        lexical_analyser.acceptCharacter("(")
        expression(lexical_analyser)
        lexical_analyser.acceptCharacter(")")
        logger.debug("Call to put")
        # * Point de génération du code
        if used_var:
            compiler.put()
            used_var = False
        else:
            logger.error("Valeur non initialisée!")
            raise AnaSynException("Valeur non initialisée!")
    else:
        logger.error("Unknown E/S instruction!")
        raise AnaSynException("Unknown E/S instruction!")


def boucle(lexical_analyser):
    global type_sommet_pile
    global operation
    logger.debug("parsing while loop: ")
    # ? while < C > loop
    # ?       < A >
    # ? end

    # ad1 : { <C> }
    # tze(ad2) ;
    # { <A> }
    # tra(ad1) ;
    # ad2 :

    lexical_analyser.acceptKeyword("while")

    # ad1 au quel on ajoute loop_counter pour sauter les tze au dessus
    # et callable_counter pour sauter les tra des procédures fonctions
    ad_1 = (
        len(compiler.instructions)
        + compiler.loop_counter
        + compiler.callable_counter
        + 1
    )

    # ? Compte les boucles imbriquées
    # ? Autrement dit le nombre de tze de loop qu'il y aura dans le code objet
    counter = compiler.loop_counter
    compiler.loop_counter += 1

    expression(lexical_analyser)

    lexical_analyser.acceptKeyword("loop")

    # ? On stocke la position ou on va insérer le tze après exécution de <A>
    pos_1 = len(compiler.instructions)

    suiteInstr(lexical_analyser)

    # on ajoute 1 pour passer à l'instruction après la suite d'instructions
    # on ajoute 1 pour sauter le tra
    # on ajoute 1 pour sauter le tze
    # on ajoute counter pour sauter les boucles while
    # on ajoute altern_counter pour sauter les alternatives
    # on ajoute callable_counter pour sauter les tra des procédures/fonctions
    ad_2 = (
        len(compiler.instructions)
        + 1
        + 1
        + 1
        + counter
        + compiler.altern_counter
        + compiler.callable_counter
    )
    compiler.tra(ad_1)
    compiler.put_instruction(pos_1, ["tze", ad_2])
    lexical_analyser.acceptKeyword("end")
    logger.debug("end of while loop ")
    compiler.loop_counter -= 1
    if len(operation)==0:
        logger.error("La condition de boucle doit être booléenne!")
        raise AnaSynException("La condition de boucle doit être booléenne!")
    if compiler.loop_counter<len(operation):
        operation.pop()


def altern(lexical_analyser):
    global operation
    logger.debug("parsing if: ")
    lexical_analyser.acceptKeyword("if")
    # ? Cas d'alternatives
    # ? if < C > then
    # ?     < A >
    # ? end
    expression(lexical_analyser)

    lexical_analyser.acceptKeyword("then")
    #var = type_sommet_pile.pop()
    
    counter = compiler.altern_counter
    compiler.altern_counter += 1
    if len(operation)==0:
        logger.error("La condition de boucle doit être booléenne!")
        raise AnaSynException("La condition if doit avoir une condition booléenne!")
    if compiler.altern_counter<len(operation):
        operation.pop()

    # ? On stocke la position ou on va insérer le tze après exécution de <C>

    # {< C >};
    pos = len(compiler.instructions)  # tze(ad);
    # {< A >};
    # ad : . . .

    suiteInstr(lexical_analyser)

    pos_2 = len(compiler.instructions)

    # On ajoute 1 pour passer à l'instruction après la suite d'instructions
    # On ajoute 1 pour sauter le tra
    tze_address = pos_2 + 1 + 1 + counter + compiler.loop_counter

    if lexical_analyser.isKeyword("else"):
        # * Si il s'agit d'une alternative double
        # ? if < C > then
        # ?     < A >
        # ? else
        # ?     < B >
        # ? end

        lexical_analyser.acceptKeyword("else")
        # ? On indique qu'il s'agit d'une alternative double
        pos_3 = len(compiler.instructions)
        # { <C> }
        # tze(ad1) ;
        # { <A> }
        # ? pos_2
        # tra(ad2) ;
        # ad1 : { <B> }
        # ad2 : . . .
        suiteInstr(lexical_analyser)
        pos_4 = len(compiler.instructions)
        # on ajoute 1 pour passer à l'instruction après la suite d'instructions
        # on ajoute 1 pour sauter le tra
        # on ajoute 1 pour sauter le tze
        tra_address = pos_4 + 1 + 1 + 1 + compiler.callable_counter
        compiler.put_instruction(pos_3, ["tra", tra_address])
        tze_address += 1
        tze_address += compiler.callable_counter

    compiler.put_instruction(pos, ["tze", tze_address])

    lexical_analyser.acceptKeyword("end")
    logger.debug("end of if")
    compiler.altern_counter -= 1


def retour(lexical_analyser):
    logger.debug("parsing return instruction")
    lexical_analyser.acceptKeyword("return")
    expression(lexical_analyser)
    # * Point de génération du code
    compiler.retour_fonct()


########################################################################
def main():

    showPile = False
    parser = argparse.ArgumentParser(
        description="Do the syntactical analysis of a NNP program."
    )
    parser.add_argument(
        "inputfile", type=str, nargs=1, help="name of the input source file"
    )
    parser.add_argument(
        "-o",
        "--outputfile",
        dest="outputfile",
        action="store",
        default="",
        help="name of the output file (default: stdout)",
    )
    parser.add_argument("-v", "--version", action="version", version="%(prog)s 1.0")
    parser.add_argument(
        "-d",
        "--debug",
        action="store_const",
        const=logging.DEBUG,
        default=logging.INFO,
        help="show debugging info on output",
    )
    parser.add_argument(
        "-p",
        "--pseudo-code",
        action="store_const",
        const=True,
        default=False,
        help="enables output of pseudo-code instead of assembly code",
    )
    parser.add_argument(
        "--test-unit",
        action="store_true",
        help="prints the list containing instructions",
    )
    parser.add_argument(
        "--show-ident-table",
        action="store_true",
        help="shows the final identifiers table",
    )
    parser.add_argument(
        "--show-instructions",
        action="store_true",
        help="shows the final code instructions",
    )
    parser.add_argument(
        "--execution",
        action="store_true",
        help="Execute code",
    )
    parser.add_argument(
        "--show-pile",
        action="store_true",
        help="Execute code",
    )
    args = parser.parse_args()

    filename = args.inputfile[0]

    f = None
    try:
        # ? encoding évite les erreurs de lecture
        f = open(filename, "r", encoding="utf-8", errors="ignore")
    except:
        print("Error: can't open input file!")
        return

    outputFilename = args.outputfile

    # create logger
    LOGGING_LEVEL = args.debug
    logger.setLevel(LOGGING_LEVEL)
    ch = logging.StreamHandler()
    ch.setLevel(LOGGING_LEVEL)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    if args.pseudo_code:
        True  #
    else:
        False  #

    lexical_analyser = analex.LexicalAnalyser()

    lineIndex = 0
    for line in f:
        line = line.rstrip("\r\n")
        lexical_analyser.analyse_line(lineIndex, line)
        lineIndex = lineIndex + 1
    f.close()

    # launch the analysis of the program
    lexical_analyser.init_analyser()
    program(lexical_analyser)

    if args.show_ident_table:
        identifier_table = compiler.print_identifier_table()
        print("------ IDENTIFIER TABLE ------")
        print(identifier_table)
        print("------ END OF IDENTIFIER TABLE ------")

        print("\n\n")

    if args.test_unit:
        test_instructions = compiler.print_test()
        return test_instructions
    if args.show_pile:
        showPile = True
    if args.execution:
        print("------ EXECUTION ------")
        vm = VirtualMachine(compiler.identifier_table, showPile)
        vm.addInstructionList(compiler.instructions)
        vm.execute()
        print("------ END OF EXECUTION ------")
        print("\n\n")

    if args.show_instructions:
        print("------ INSTRUCTIONS ------")
        show_instructions = compiler.print_instructions()
        print(show_instructions)
        print("------ END OF INSTRUCTIONS ------")

        print("\n\n")

    if outputFilename != "":
        try:
            output_file = open(outputFilename, "w")
        except:
            print("Error: can't open output file!")
            return
    else:
        output_file = sys.stdout

    # Outputs the generated code to a file
    # instrIndex = 0
    # while instrIndex < codeGenerator.get_instruction_counter():
    #        output_file.write("%s\n" % str(codeGenerator.get_instruction_at_index(instrIndex)))
    #        instrIndex += 1

    if outputFilename != "":
        output_file.close()


########################################################################

if __name__ == "__main__":
    main()
