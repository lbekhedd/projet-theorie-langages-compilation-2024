"""
 # @ Author: Barbara Abid-Braga
 # @ Modified time: 2024-05-05 01:16:00
 # @ Description: Virtual Machine package.
 """

import ctypes
from typing import List
from analex import Identifier
from tableid import IdentifierTable
import re


class VirtualMachine:
    def __init__(self,table,showPile):
        self.pile = [] #initialisation de la pile
        self.instructions = [] # la liste d'instruction utilisé
        self.vmUtility = VirtualMachineUtility(table) # fonction utile à  la VM
        self.affect = [] # tableau buffer servant à communiquer les valeurs sur le type, l'addresse et le numéro de bloc de la variable en cours de traitement
        self.numero_ligne = 0 # sert au suivit de numéro de ligne 
        self.returnFonc = False # variable signalant si il y eu un retour pour activer le tra
        self.blocId = 0 # variable contenant le numéro de bloc actuel
        self.lignes = [] # tableau contenant toute les ligen par laquel il est passé durant l'execution
        self.traStatActive = False # variable pour dire si il y a un traStat actif ou non
        self.traActive = False # variable pour savoir si on doit faire un saut ( conditionnel ou non ) 
        self.erreurDescription = "" # tableau contenant toute les erreurs
        self.typeValue = ""
        self.valeurBool =False # valeur indiquant si on a affaire à un booléen
        self.table = table # table des identificateur
        self.buff = []
        self.showPile = showPile

    def addInstructionList(self,instructions): 
        """Prend la liste d'instruction venant du compilateur et la traduit en un tableau de fonction executable"""
        for instruction in instructions: # boucle dans la liste d'instruction venant du compilateur
            match instruction[0]: # switch sur l'instruction
                case "mult": # si on veut une multiplication
                    self.addInstruction(self.mult,[],None)
                case "add": # si on veut une addition
                    self.addInstruction(self.add,[],None)
                case "affectation": # si on veut une affectation
                    self.addInstruction(self.affectation,[],None)
                case "get": # si on veut un get
                    self.addInstruction(self.get,[],None)
                case "valeurPile": # si on veut empiler une variable de la pile
                    self.addInstruction(self.valeurPile,[],None)
                case "reserverBloc": # si on veut reserver un bloc
                    self.addInstruction(self.reserverBloc,[],None)
                case "retourFonct": # si on veut faire un retour de fonction
                    self.addInstruction(self.retourProc,[],None)
                case "debutProg": # si on veut commencer le programme
                    self.addInstruction(self.debutProg,[],None)
                case "finProg": # si on veut finir le programme
                    self.addInstruction(self.finProg,[],None)
                case "put": # si on veut afficher une variable
                    self.addInstruction(self.put,[],None)
                case "moins": # si on veut rendre négatif une variable
                    self.addInstruction(self.moins,[],None)
                case "sous": # si on veut faire une soustrattion
                    self.addInstruction(self.sous,[],None)
                case "div": # si on veut faire une division
                    self.addInstruction(self.div,[],None)
                case "diff": # si on veut faire une difference
                    self.addInstruction(self.diff,[],None)
                case "inf": # si on veut utiliser l'opérateur inférieur
                    self.addInstruction(self.inf,[],None)
                case "infeg": # si on veut utilisé l'opérateur de infériorité ou d'égalité
                    self.addInstruction(self.infeg,[],None)
                case "sup": # si on veut utiliser l'opérateur de supériorité
                    self.addInstruction(self.sup,[],None)
                case "supeg": # si on veut utiliser l'opérateur de supériorité ou d'égalité
                    self.addInstruction(self.supeg,[],None)
                case "ou": # OU logique
                    self.addInstruction(self.ou,[],None)
                case "et": # et logique
                    self.addInstruction(self.et,[],None)
                case "erreur": # si on veut lister les erreurs
                    self.addInstruction(self.erreur,[],None)
                case "non": # contraire d'une valeur booléenne
                    self.addInstruction(self.non,[],None)
                case "egal": # si on veut utilisé l'opérateur de égalité
                    self.addInstruction(self.egal,[],None)
                case "retourConstr": #si on veut faire un retoru de constructeur
                    self.addInstruction(self.retourConstr,[],None)
                case "retourProc": # si on veut faire un retour de procédure
                    self.addInstruction(self.retourProc,[],None)
                case "empiler": # si on veut empiler une valeur ou une addresse
                    self.addInstruction(self.empiler,[instruction[1]],instruction[2])
                case "empilerParam": # si on veut empiler paramètres
                    self.addInstruction(self.empilerParam,[instruction[1]],None)
                case "reserver": # si on veut réserver une variable dans un bloc
                    self.addInstruction(self.reserver,[instruction[1]],None)
                case "tze": # si on veut faire un saut conditionel
                    self.addInstruction(self.tze,[instruction[1]],None)
                case "tra": # si on veut faire un saut inconditionel
                    self.addInstruction(self.tra,[instruction[1]],None)
                case "traStat": # si on veut faire un saut inconditionel avec paramètre
                    self.addInstruction(self.traStat,[instruction[1],instruction[2]],None)
                case "empilerAd":
                    self.addInstruction(self.empilerAd,[instruction[1]],None)
                case _: # sinon la VM ne contient pas la fonction
                    print(instruction[0])
                    print("Instructions inconnus.")
    def addInstruction(
        self, instruction, parametres, type_val
    ):  
        """Permet de ajouter une instruction dans la liste d'instruction avec ses paramètre"""
        
        if len(parametres) == 0: # si la fonction ne contient aucun paramètre
            self.instructions.append({"fonction": instruction, "nbParam": 0,"type":type_val}) # initialisation de l'instruction
        if len(parametres) == 1: # si la fonction possède 1 paramètre
            self.instructions.append(
                {"fonction": instruction, "parametre": parametres[0], "nbParam": 1,"type":type_val}# initialisation de l'instruction
            )
        if len(parametres) == 2: # si la fonction possède 2 paramètre
            self.instructions.append(
                {
                    "fonction": instruction,
                    "parametre1": parametres[0],
                    "parametre2": parametres[1],
                    "nbParam": 2,
                    "type":type_val,
                }# initialisation de l'instruction
            )

    def execute(self): 
        """Execute la liste d'instruction"""
        ligneTraStat = [] # ligne de sauvegarde du tra ou traStat
        ligneTra = -1
        retournProcRetenu = False
        index = 0 # numéro de ligne en cours
        while index < len(self.instructions): # boucle dans la liste d'instruction
            item = self.instructions[index] # récupération de l'instruction à éxécuter
            if self.showPile:
                print("Ligne N°:",index)
                print("Instruction :")
            if self.traStatActive == True: # si on a un traStat qui a été lancé
                ligneTraStat.append(index - 1) # on retient sa ligne
                index = self.lignes[-1] # saut inconditionel avec paramètre
                self.traStatActive = False # on désactive car on a fait les préparation suffisante
            elif self.traActive == True: # si on a tze ou tra actif 
                ligneTra = index - 1 # on retient sa ligne
                index = self.lignes[-1] # saut
                self.traActive = False # on désactive car on a fait les préparation suffisante
            elif self.returnFonc == True: # si on a un retour de fonction ou de procédure
                self.returnFonc = False
                index = ligneTraStat[-1] +1# aller à la ligne de traStat ( là où on a fait l'appelle de fonction/procédure )
                ligneTraStat.pop()
            elif self.traStatActive == False:
                if len(ligneTraStat)!=0: # si on a pas de traStat en cours ( donc pas de fonction nin de recursion à part le main)
                    if index != ligneTraStat[-1]: # si toute les préparation a été effectué  on execute une instruction
                        if item["nbParam"] == 1: # si il ne possède 1 paramètre
                            fonction = self.empiler # si on veut empiler on cherche à savoir si c'est une adresse ou un entier ou un booléen
                            if item["fonction"] == fonction:
                                self.typeValue = item["type"]
                            item["fonction"](item["parametre"])  # execution
                        if item["nbParam"] == 0:# si il ne possède 0 paramètre
                            item["fonction"]() # execution
                        if item["nbParam"] == 2: # si il ne possède 2 paramètre
                            item["fonction"](item["parametre1"], item["parametre2"]) # execution
                    index += 1 # incrémentation du numéro de ligne
                    if self.showPile:
                        print("******** PILE ********")
                        print(self.pile)
                else: # si on a un traStat et donc une récursion ou juste une fonction
                    if item["nbParam"] == 1: # si il ne possède 1 paramètre
                        fonction = self.empiler # si on veut empiler on cherche à savoir si c'est une adresse ou un entier ou un booléen
                        if item["fonction"] == fonction:
                            self.typeValue = item["type"]
                        item["fonction"](item["parametre"])  # execution
                    if item["nbParam"] == 0:# si il ne possède 0 paramètre
                        item["fonction"]() # execution
                    if item["nbParam"] == 2: # si il ne possède 2 paramètre
                        item["fonction"](item["parametre1"], item["parametre2"]) # execution
                    index += 1 # incrémentation du numéro de ligne
                    if self.showPile:
                        print("******** PILE ********")
                        print(self.pile)
            else:
                index+=1

    def erreur(self):
        """Affichage des erreurs si besoin est"""
        raise Exception(self.erreurDescription) # affichage de l'erreur
    
    def empiler(self, valeur):
        """Empiler une addresse ou une valeur"""
        if self.showPile: # si on veut afficher la pile
            print("empiler")
        if self.typeValue=="adresse": # si on veut empiler une adresse
            self.affect.append([self.blocId,valeur+1])# signaler qu'elle est et enregistré son bloc id et son addresse
        elif self.typeValue=="boolean": # si on veut empiler un booléen
            self.pile.insert(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)+1,(valeur==1)) # on empile le booléen à la fin du bloc en cours
            self.pile = self.vmUtility.update_adress(self.pile, self.blocId, self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1, 1) # update de toute les addresse
        else: # si on veut empiler un entier
            self.pile.insert(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)+1,valeur)
            self.pile = self.vmUtility.update_adress(self.pile, self.blocId, self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1, 1) # update de toute les addresse
        self.lignes.append(self.numero_ligne) # ajout de la ligne en cours
        self.numero_ligne += 1 # incrémentation du numéro de ligne
    
    def empilerAd(self,ad):
        """Empiler une adresse dynamique"""
        if self.showPile:
            print("empilerAd")
        ad = ad+self.vmUtility.get_adresse_debutBloc(self.pile,self.blocId)
        self.affect.append([self.blocId,ad]) # signaler qu'elle est et enregistré son bloc id et son addresse
        self.lignes.append(self.numero_ligne) # ajout de la ligne en cours
        self.numero_ligne += 1 # incrémentation du numéro de ligne

    def empilerParam(self, ad):
        """empiler un paramètre de procédure ou de fonction"""
        if self.showPile:
            print("empilerParam")
        self.pile = self.vmUtility.parametre_add(self.pile, self.blocId, ad) # ajout du paramètre dans la liste des paramètres
        self.lignes.append(self.numero_ligne) # rajouter une ligne à éxecuter
        self.numero_ligne += 1 # incrémenter la ligne en cours

    def mult(self):
        """Opération de multiplication de la VM"""
        if self.showPile: # si on veut afficher la pile
            print("mult")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le sommet de la pile qui est le premier element 
        b = self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] # prendre le deuxième element au sommet de la pile
        c = a * b # effectuer la multiplication
        self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1]=c # modifier le sommet de pile
        self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # update de toute les addresse
        self.lignes.append(self.numero_ligne) # ajouter le numéro de ligne
        self.numero_ligne += 1 # incrémenter le numéro de ligne actuel

    def add(self):
        """Opération d'addition"""
        if self.showPile:
            print("add")
        
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le dernier element du bloc en cours
        b = self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] # prendre le nouveau dernier element en cours
        c = a + b # effectuer l'addition
        self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1]=c # modifier le sommet de pile
        self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # update toute les addresses 
        self.lignes.append(self.numero_ligne) # ajouter le numéro de ligne
        self.numero_ligne += 1 # incrémenter le numéro de ligne actuel

    def affectation(self):
        """Opération d'affectation"""
        if self.showPile:
            print("affectation")
        buffPile = [] # buffer de vérification pour savoir si l'opération d'affection se fait de façon conforme
        if len(self.affect)==0: # si la queu d'affection est vide
            self.affect.append(self.buff) # on l'a rempli avec le dernier element enlever
        address_variable = self.affect[-1][1] # addresse dynamique de la variable en cours
        buffPile,self.table = self.vmUtility.variable_modify(
                self.pile,
                self.affect[-1][0],
                address_variable,
                self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # modifier la variable en cours
        self.pile = buffPile
        self.buff = self.affect.pop() # enlever l'identificateur de la queu des identificateur
        self.lignes.append(self.numero_ligne) # ajout du numéro de ligne
        self.numero_ligne += 1 # incrémentation du numéro de ligne 

    def valeurPile(self):
        """Empiler une valeur de variable"""
        if self.showPile:
            print("valeurPile")
        if len(self.affect) ==0: # si on a pas de variable à affecter
            self.pile.append(self.pile[-1]) # on empile le sommet
            self.pile = self.vmUtility.update_adress(self.pile, self.blocId, 0, 1) # on update les addresses
        else: # si on a une variable ciblé
            valeur = self.pile[self.affect[-1][1]] # récuperer l'adresse dynamique de la variable
            self.pile.insert(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)+1,valeur) # ajoute rla valeur à la pile pour l'affectation
            self.pile = self.vmUtility.update_adress(self.pile, self.blocId, 0, 1) # utpdate toute les variables
        if len(self.affect)>0:
                self.buff = self.affect.pop()
        self.lignes.append(self.numero_ligne) # ajout du numéro de ligne
        self.numero_ligne += 1 # incrémentation du numéro de ligne

    def get(self):
        """Permet l'entré utilisateur d'une valeur entière ou booléenne"""
        if self.showPile:
            print("get")
        buffPile = []
        valeur= input("Entrez un entier: ") # demande à l'utilisateur de entrer une valeur
        if bool(re.match(r'^-?\d+$', valeur)): # si l'utilisateur à entré un entier
            valeur = int(valeur) # en faire un entier
        else: # si c'est un string quelquonque
            errorMsg = "Ligne N°",self.numero_ligne," : La valeur entrée doit être du type entier."
            self.erreurDescription = errorMsg
            self.erreur()
        adress_variable = 0 # addresse de la variable que l'on veut affectuer d'une valeur
        #ad = self.vmUtility.get_adress_variable(self.pile,self.blocId) # avoir toute les addresses des variables
        if len(self.affect)==0: # si on plus de valeur dans la queu 
            self.affect.append(self.buff) # réaffecté l'ancienne valeur voulu
        self.pile.insert(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)+1,valeur) # ajoute rla valeur à la pile pour l'affectation
        self.pile = self.vmUtility.update_adress(self.pile, self.blocId, self.vmUtility.get_adresse_debutBloc(self.pile,self.blocId), 1) # utpdate toute les variables
        buffPile,self.table = self.vmUtility.variable_modify(
                self.pile,
                self.affect[-1][0],
                self.affect[-1][1],
                self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # modifier la variable en cours
        self.pile = buffPile # mettre à jour la pile
        self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le buffer inutile
        self.pile = self.vmUtility.update_adress(self.pile, self.blocId, self.vmUtility.get_adresse_debutBloc(self.pile,self.blocId), -1) # utpdate toute les variables
        self.buff = self.affect.pop() # enlever la variable dans la queu 
        self.lignes.append(self.numero_ligne) # ajout du numéro de ligne
        self.numero_ligne += 1 # incrémentation de la ligne en cours
    
    def moins(self):
        """rendre négatif une variable"""
        if self.showPile:
            print("moins")
        self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)]=-self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)]# ajouter un moins au sommet de pile
        self.lignes.append(self.numero_ligne) # ajouter le numéro de pile
        self.numero_ligne += 1 # incrémenter le numéro de pile
    
    def sous(self):
        """Faire une soustraction"""
        if self.showPile:
            print("sous")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le sommet de la pile qui est le premier element 
        b = self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] # prendre le deuxième element au sommet de la pile
        c = b-a # effectuer la soustraction
        self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1]=c #modifier le sommet de pile
        self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # décrémenter les addresse necessaire
        self.lignes.append(self.numero_ligne) # rajout de la ligne en cours dans la liste des ligne à executer
        self.numero_ligne += 1
        
    def div(self):
        """Faire une division"""
        if self.showPile:
            print("div")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le sommet de la pile qui est le premier element 
        b = self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] # prendre le deuxième element au sommet de la pile
        c = b/a # effectuer la division entière
        self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1]=int(c) # mettre la valeur entière de c dans le sommet de pile
        self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # décrémenter les addresses necessaire
        self.lignes.append(self.numero_ligne) # ajoute rle numéro de ligne
        self.numero_ligne += 1# incrémenter le numéro de ligne

    def put(self):
        """Afficher une variable"""
        if self.showPile:
            print("put")
        if isinstance(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)],type(None)): # verifier si on a bien définie la variable voulue
            errorMsg = "Ligne N°",self.numero_ligne," : L'identificateur n'existe pas au sein du bloc ou n'est pas initialisé."
            self.erreurDescription = errorMsg
            self.erreur() # erreur d'affectation
        else: # si l'identificateur existe au sein du bloc
            print(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)]) # afficher l'identificateur correspondant
        self.lignes.append(self.numero_ligne) # incrémenter le sommet de pile
        self.numero_ligne += 1 # incrémenter le numéro de ligne en cours

    def tze(self, ligne):
        """Faire un saut conditionel"""
        if self.showPile:
            print("tze")
        self.traActive = True # montrer que on va faire un saut 
        if self.pile[-1] == False: # si elle est fausse alors on arrète la boucle
            self.numero_ligne =ligne - 1  # on va à la fin de la boucle
            self.lignes.append(self.numero_ligne) # on ajoute le numéro e ligne
            self.numero_ligne += 1 # on incrémente le numéro de ligne actuelle
        else: # si elle est true alors on entre dasn la boucle
            self.lignes.append(self.numero_ligne) # on ajoute le numéro de ligne
            self.numero_ligne += 1 # on incrémente le numéro de ligne actuelle

    def tra(self, ligne):
        """Faire un saut inconditonel"""
        if self.showPile:
            print("tra")
        self.traActive = True # dire que on veut faire un saut
        self.numero_ligne = ligne -1# ajouter le numéro de ligne
        self.lignes.append(self.numero_ligne) # ajouter le numéro de ligne 
        self.numero_ligne += 1 # incrémenter le numéro de ligne actuelle

    def traStat(self, ligne, parametre):
        """Faire un saut inconditonel avec un ou plusieur paramètre"""
        if self.showPile:
            print("traStat")
        self.traStatActive = True # dire que on veut faire un saut avec des paramètre en plus
        self.numero_ligne = ligne -1 # mettre à jour le preochain numéro de ligne
        parametres = []
        if parametre!=0:
            self.pile = self.vmUtility.parametre_add(
                self.pile, self.blocId+1, parametre
            ) # ajoute les paramètres de la procédure ou de la fonction
            sup = 0
            if parametre>1:
                sup=-1
            for i in range(0,parametre): # enregistrer les parametre
                self.pile.append(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId+1)-parametre])
                self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)+sup)
        self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -parametre,
        ) # décrémenter les addresses necessaire
        self.blocId+=1 # changement de bloc
        self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            parametre,
        ) # décrémenter les addresses necessaire
        self.lignes.append(self.numero_ligne) # ajouter le numéro de ligne
        self.numero_ligne += 1 # incrémenter le numéro de ligne

    def reserverBloc(self):
        """Reserver une base"""
        if self.showPile:
            print("reserver Bloc")
        parametres = [] # initialisation des paramètres
        self.pile = self.vmUtility.alloc_base(self.pile, parametres) # alouer une base
        self.lignes.append(self.numero_ligne) # ajouter le numéro de ligne
        self.numero_ligne += 1 # incrémenter le numro de ligne en cours

    def retourProc(self):
        """Retour de fonction"""
        if self.showPile:
            print("retournFonct")
        self.blocId-=1 # décrémenter le bloc actuel pour revenir au précedent
        indice = 0 # initialisation de l'indice de la valeur de retour
        if isinstance(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId+1)],dict): # vérifier si on pointe pas sur une fin de bloc
            indice = self.vmUtility.get_adresse_finBloc(self.pile,self.blocId+1)-1 # on recule de un
        else:
            indice = self.vmUtility.get_adresse_finBloc(self.pile,self.blocId+1) # sinon on prends celui en cours
        self.pile.insert(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)+1,self.pile[indice]) # insertion de la valeur de retour à la fin du bloc précedent pour de futur traitement
        self.pile = self.vmUtility.update_adress(self.pile, self.blocId, 0, 1) # update du bloc actuel
        self.pile = self.vmUtility.update_adress(self.pile, self.blocId+1, 0, 1) # update du bloc précédent
        self.pile = self.vmUtility.set_valeur_retour(
            self.pile, self.blocId+1, self.pile[indice+1]
        ) # set le retour avec le sommet de pile qui est le résultat de la fonction
        self.lignes.append(self.numero_ligne) # ajouter le numéro de ligne
        self.numero_ligne += 1 # incrémenter le numéro de ligne en cours
        self.returnFonc = True # dire que on a bien un retour et qu'on peut sortir de la fonction

    def debutProg(self):
        """Debut du programme en initialisant la base du main"""
        if self.showPile:
            print("debut Prog ")
        self.pile = self.vmUtility.alloc_base(self.pile, []) # allouer la base du main
        self.lignes.append(self.numero_ligne) # ajouter le numéro de ligne
        self.numero_ligne += 1 # incrémenter le numéor de ligne en cours

    def finProg(self):
        """Fonction signalant la fin du programme"""
        if self.showPile:
            print("finProg")
        self.lignes.append(self.numero_ligne) # ajouter le numéro de ligne
        self.numero_ligne += 1 # incrémenter le numéro de ligne en cours

    def diff(self):
        """Vérifier si deux valeurs sont différentes ou non"""
        if self.showPile:
            print("diff")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le sommet de ligne qui est le premier element
        if type(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1])==type(a): # varifier que ils sont du même type
            self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] = (self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] != a) # empiler le résultat de la vérification de la différence
            self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # update les addresse necessaire
        else: # si ils sont pas du même type
            errorMsg ="Ligne : ",self.numero_ligne, " : Les deux valeurs de la différence ne sont pas du même types."
            self.erreurDescription = errorMsg
            self.erreur() # erreur d'affectation
        self.lignes.append(self.numero_ligne) # ajout du numéro de ligne
        self.numero_ligne += 1 # incrémentation du numéro de ligne en cours

    def egal(self):
        """Opérateur d'égalité entre deux variables"""
        if self.showPile:
            print("egal")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le dernier element du bloc en cours
        if type(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1])==type(a): # varifier si les deux variables sont de même type
            self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] = (self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] == a) # empiler le résultat de l'égalité
            self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # décrémenter les addresses necessaires
        else:
            errorMsg ="Ligne N°",self.numero_ligne, " : Les deux valeurs de l'égalité ne sont pas du même types !"
            self.erreurDescription = errorMsg
            self.erreur() # erreur d'affectation
        self.lignes.append(self.numero_ligne)
        self.numero_ligne += 1
    
    def inf(self):
        """Opérateur d'infériorité entre deux variables"""
        if self.showPile:
            print("inf")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le sommet de ligne qui est premier elements
        if type(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1])==type(a): # vérifier que les deux variables sont du même type
            self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] = (self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] < a) # ajouter le résultat de l'opération d'infériorité
            self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) 
        else:
            errorMsg ="Ligne : ",self.numero_ligne, " : L'opération d'infériorité se fait avec des valeurs de types différence ."
            self.erreurDescription = errorMsg
            self.erreur() # erreur d'affectation
        self.lignes.append(self.numero_ligne)
        self.numero_ligne += 1
    
    def infeg(self):
        """Opérateur d'infériorité ou d'égalité entre deux variables"""
        if self.showPile:
            print("infeg")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le dernier element du bloc en cours
        if type(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1])==type(a): # vérifier qu'ils sont du même type
            self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] = (self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] <= a) # rajout du résultat de la comparaison
            self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # update de la pile
        else:
            errorMsg ="Ligne N° ",self.numero_ligne, " : L'opération inférieur ou égale se fait avec des valeurs de types différence !"
            self.erreurDescription = errorMsg
            self.erreur() # erreur d'affectation
        self.lignes.append(self.numero_ligne)
        self.numero_ligne += 1
    
    def supeg(self):
        """Opérateur de supériorité ou d'égalité entre deux variables"""
        if self.showPile:
            print("supeg")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le dernier element du bloc
        if type(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1])==type(a): # vérifier que les deux variable sont bien du bon type
            self.pile.append(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] >= a) # rajout à la pile du résultat de la pile
            self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # update des adresses de la pile
        else:
            errorMsg ="Ligne : ",self.numero_ligne, " : L'opération supériorité se fait avec des valeurs de types différence."
            self.erreurDescription = errorMsg
            self.erreur() # erreur d'affectation
        self.lignes.append(self.numero_ligne) # rajout de la ligne à executer
        self.numero_ligne += 1 # incrémentation du numéro de ligne en cours
    
    def sup(self):
        """Opérateur de supériorité entre 2 variables"""
        if self.showPile:
            print("sup")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le dernier element du bloc
        if type(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1])==type(a): # vérifier que les deux variable sont bien du bon type
            self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] = (self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] > a)  # rajout à la pile du résultat de la pile
            self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # update des adresses dans la pile
        else:
            errorMsg ="Ligne : ",self.numero_ligne, " : L'opération de supériorité se fait avec des valeurs de types différence."
            self.erreurDescription = errorMsg
            self.erreur() # erreur de type
        self.lignes.append(self.numero_ligne)
        self.numero_ligne += 1

    def et(self):
        """Opération logique ET entre deux valeurs booléenne"""
        if self.showPile:
            print("et")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le dernier element du bloc
        if type(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1])==type(a): # vérifier que les deux variable sont bien du bon type
            self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] = (self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] and a) # rajout à la pile du résultat de la pile
            self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # update des adresses de la pile
        else:
            errorMsg ="Ligne N° ",self.numero_ligne, " : L'opération ET logique se fait avec une ou plusieurs valeur non booléennes !"
            self.erreurDescription = errorMsg
            self.erreur() # erreur 
        self.lignes.append(self.numero_ligne) # rajout du numéro de ligne 
        self.numero_ligne += 1 # incrémentation du numro de ligne en cours
    
    def ou(self):
        """Opération logique OU entre deux valeur booléenne"""
        if self.showPile:
            print("ou")
        a = self.pile.pop(self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)) # enlever le dernier element du bloc
        if type(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1])==type(a): # vérifier que les deux variable sont bien du bon type
            self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] = (self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] or a) # rajout à la pile du résultat de la pile
            self.pile = self.vmUtility.update_adress(
            self.pile,
            self.blocId,
            self.vmUtility.get_adresse_debutBloc(self.pile, self.blocId),
            -1,
        ) # update des adresses de la pile
        else:
            errorMsg ="Ligne N° ",self.numero_ligne, " : L'opération OU logique se fait avec une ou plusieurs valeur non booléennes !"
            self.erreurDescription = errorMsg
            self.erreur() # erreur d'affectation
        self.lignes.append(self.numero_ligne) # rajout du numéro de ligne 
        self.numero_ligne += 1  # incrémentation du numro de ligne en cours

    def non(self):
        """Opération NOT d'une valeur booléene"""
        if self.showPile:
            print("non")
        if isinstance(self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1],bool): # varification que la velur est bien du type booléen
            self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1]= not self.pile[self.vmUtility.get_adresse_finBloc(self.pile,self.blocId)-1] # modification de la velur booléen par son contraire
        else: # si pas du type booléen alors on affiche une erreur
            errorMsg ="Ligne N° ",self.numero_ligne, " :  L'opération NON logique se fait sur une valeur non booléenne !"
            self.erreurDescription = errorMsg
            self.erreur() # erreur d'affectation
        self.lignes.append(self.numero_ligne)
        self.numero_ligne += 1

    def reserver(self, nombreVariable):
        """Reservation d'une place pour une variable dans la pile"""
        if self.showPile:
            print("reserver")
        if nombreVariable>=0: # si le nombre de variable à réserver est bien supérieur à 0
            for i in range(0, nombreVariable): # boucle de réservation de variable dans la pile en fonction du bloc
                self.pile,self.table = self.vmUtility.variable_add(self.pile, self.blocId) # rajout de variable avec vérification dans la table des identificateur
            self.pile = self.vmUtility.update_adress(
                self.pile, self.blocId, 0, nombreVariable
            ) # update des adresse dans le bloc en cours
            self.lignes.append(self.numero_ligne)
            self.numero_ligne += 1
        else:
            errorMsg="Ligne N° ",self.numero_ligne," :Il y a aucune variable déclarer!"
            self.erreurDescription = errorMsg
            self.erreur() # erreur 

class VirtualMachineUtility:
    """Classe permettant la gestion et la vérification de la pile et la modification de la table des identificateur"""
    def __init__(self,table) -> None:
        self.blocId=0 # initialisation du bloc en cours
        self.blocFonc=[] # tableau contenant toute les bases des fonction du programme
        self.table = table # contient la table des identificateur
        
    def alloc_base(self,pile,parametres):
        """Allocation de base pour procédure ou fonction"""
        base = {} # initialisation de la base
        self.id_type = "procedure" # allocationd du type
        base["foncId"]=self.blocId # ajout de l'id 
        base["parametres"] = parametres #ajout des paramètres
        base['ligneFin']=len(pile) # ajout de la cellule de fin dans la pile
        base["ligneDebut"]=len(pile) # ajout de la cellule de fin de la pile
        base["variablesAdd"]=[] # initialisation de la base
        base["retour"]=0 # initalisation de la valeur de retour
        if len(self.blocFonc)!=0:
            base["IP"]=self.blocFonc[-1]["ligneDebut"] 
        else: # si on affaire au main
            main={}
            id_main = "main"
            main["foncId"]=0
            main["parametres"] = []
            main['ligneFin']=len(pile)
            main["ligneDebut"]=len(pile)
            main["variablesAdd"]=[]
            self.blocFonc.append(main)
            base["IP"]=self.blocFonc[-1]["ligneDebut"]
        self.blocFonc.append(base) # rajout de la base à la liste des base
        pile.append(base)
        self.blocId=self.blocId+1
        return pile

    def update_adress(self,pile,numberBloc,adStatDebut,up):
        """Update les addresses de la pile en fonction de l'adresse de modification"""
        for base in pile: # boucle pour la recherche de base dans la pile
            if isinstance(base,dict): # si on trouve une base
                if base["foncId"] == numberBloc: # si on est dans le bon bloc
                    base["ligneFin"]=base["ligneFin"]+up # update de la fin du blocs
                    base["IP"]=base["IP"]+up 
                if base["foncId"] > numberBloc: # si le bloc au dessus de la base actuelle
                    # mettre à  jour toute les bases au dessus
                    base["ligneFin"]=base["ligneFin"]+up
                    base["ligneDebut"]=base["ligneDebut"]+up
                    base["IP"]=base["IP"]+up
        return pile

    def variable_add(self,pile,numberBloc):
        """Initialisation de variable en fonction de la table des identificateurs"""
        i=0 # indice de la base cherché dans la pile
        ad = 0 # adresse de la variable en cas d'erreur
        if isinstance(pile,type(None))==False:
            for base in pile: # boucle pour la recherche de base dans la pile
                if isinstance(base,dict): # si on a trouvé une pile
                    if base["foncId"]==numberBloc: # si on a trouvé la base du bloc en cours
                        base["variablesAdd"].append(len(base["variablesAdd"])) # ajout de 
                        pile[i]=base # modification de la base
                        id = self.table.find_identifier_by_address(base["variablesAdd"][-1],numberBloc) # recherche de l'identificateur dans la base par l'adreesse
                        if isinstance(id,type(None))==False: # si on a pas trouvé l'identificateur
                            pile.insert(base["ligneDebut"]+len(base["variablesAdd"]),None) # rajout de la variable non initialisé
                        else:
                            raise Exception("La variable cherché n'existe pas au sein du bloc !") # affichage de l'erreur
                        return pile,self.table
                i+=1
        else:
            pile.insert(ad,0)
            return pile

    def parametre_add(self,pile,numberBloc,valeur):
        """Ajout de paramètre en fonction de la table des identificateur ( à faire encore)"""
        i=0 # indice de la base dans la pile
        for base in pile: # boucle pour récupérer la base dans la pile
            if isinstance(base,dict): # si on trouve une base
                if base["foncId"]==numberBloc: # si on trouve la bonne base
                    for v in range(0,valeur): # rajouter autemps de paramètre que demandé
                        base["parametres"].append(v) # rajouter à la liste des paramètres
                        base["variablesAdd"].append(len(base["variablesAdd"])) # rajouter à la liste des variables
                        pile[i]=base # modification de la base
                    break
            i+=1 # incrémentation de l'indice
        return pile

    def get_dynamic_adresse(self,pile,numberBloc,adresseStatic):
        """Recupération de l'adresse dynamique à partir de son adresse statique"""
        for base in pile: # boucle pour récuperer l'adresse dynamique de la variable
            if isinstance(base,dict): # si on trouve une base
                if base["foncId"]==numberBloc: # si on a trouvé le bon type
                    return base["ligneDebut"]+base["variablesAdd"][adresseStatic] # retour de l'adresse dynamique
        return pile
    
    def verify_indentifier(self,type1:str,scope1:str,type2:str,scope2:str): # vérification du type et de la porté et du type avant affectation
        """Vérification du scope et du type pour préparer une affectation"""
        if type1 == type2 and scope1==scope2: # si les deux variables ont la meme porté et le meme type
            return True # renvoyé que l'affecattion ets possible
        return False # renvoyer que l'affectation n'est pas possible
    def appendInBloc(self,pile,numberBloc,valeur):
        for base in pile: # boucle de recherhce dans la pile
            if isinstance(base,dict) and base["foncId"]==numberBloc: # si on trouve une base et que cette base est le bon bloc
                pile.insert(base["ligneFin"]-1) # rajouter l'élement à la derniere place
    def variable_modify(self,pile,numberBloc,adresseStatic,newAdress):
        """Modification de la variables dans la pile et dans la table des identificateurs"""
        add1 = 0 # adresse de la variable à modifier
        add2=0 # adresse de la futur valeur de la variable
        idBloc1 = 0 # id du bloc de la variable à modifier
        idBloc2=0 # id du bloc de la futur valeur à modifier
        scope1 = "" # scope de la variable à modifier
        type1= "" # type de la variable à modifier
        scope2 = "" # scope de la futur valeur à modifier
        type2 = "" # type de la futur valeur à modifier
        for base in pile: # boucle pour le parcourt de la pile
            if isinstance(base,dict): # si on a trouvé une base
                if base["ligneDebut"]<=adresseStatic and base["ligneFin"]>=adresseStatic: # si on est dans la base de la variable à modifier
                    add1 = adresseStatic - base["ligneDebut"] - 1 # récupération de son adresse dynamique
                    idBloc1 = base["foncId"] # récupération de l'id de son bloc
                if base["ligneDebut"]<=newAdress and base["ligneFin"]>=newAdress: # si on est dans la base de la futur valeur de la variable à modifier 
                    add2 = newAdress-base["ligneDebut"] # récupération de l'adresse dynamique de la futur valeur de la variable à modifier
                    idBloc2 = base["foncId"] # récuperation du bloc de la futur valeur à modifier 
        id_1 = self.table.find_identifier_by_address(add1,idBloc1) # récuperation de l'identificateur de la variable à 
        id_2 = self.table.find_identifier_by_address(add2,idBloc2)
        if isinstance(id_1,type(None))==False:
            type1 = id_1.get_type()
            scope1 = id_1.get_scope()
        else:
            raise Exception(f"L'adresse de la variable n'a pas été trouvé.")
        if isinstance(id_2,type(None))==False:
            type2 = id_2.get_type()
            scope2 = id_2.get_scope()
        else:
            if isinstance(pile[newAdress],bool):
                type2 = "boolean"
            else:
                type2 = "integer"
            tab_proc = self.table.get_name_and_adresse_procedure()
            if len(tab_proc)>=idBloc2:
                scope2 = tab_proc[idBloc2][0]
            else:
                raise Exception(f"Le scope de la valeur n'a pas été trouvé.")
        if self.verify_indentifier(type1,scope1,type2,scope2):
            pile[adresseStatic]=pile[newAdress]
            id_1.set_value(pile[newAdress])
            name = self.table.get_identifier_name_by_id(id_1,numberBloc,add1)
            self.table.update_identifier(name,id_1)
            return pile,self.table
        else:
            return self.table.get_identifier_name_by_id(id_1,len(self.blocFonc)-1,add1),self.table

    def get_adress_variable(self,pile,numberBloc):
        """récupération de toute les addresse statiques des variable du bloc considéré"""
        for base in pile:
            if isinstance(base,dict) and base["foncId"]==numberBloc:
                a = base["variablesAdd"]
                return a

    def get_adresse_finBloc(self,pile,numberBloc):
        """Recupération de l'adresse dynamique de fin de ligne"""
        
        for base in pile:
            if isinstance(base,dict) and base["foncId"]==numberBloc:
                return base["ligneFin"]
    def get_adresse_debutBloc(self,pile,numberBloc):
        """Recupération de l'adresse de début de bloc"""
        for base in pile:
            if isinstance(base,dict) and base["foncId"]==numberBloc:
                return base["ligneDebut"]+1
    def set_valeur_retour(self,pile,numberBloc,valeur):
        """Récuperation de l'adresse de retour"""
        i=0
        for base in pile:
            if isinstance(base,dict) and base["foncId"]==numberBloc:
                base["retour"] = valeur
                pile[i] = base
                break
            i+=1
        return pile