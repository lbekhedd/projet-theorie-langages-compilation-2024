"""
 # @ Author: Loueï Bekhedda
 # @ Create Time: 2024-04-30 20:28:19
 # @ Modified by: Loueï Bekhedda
 # @ Modified time: 2024-04-29 21:51:04
 # @ Description: Compiler package.
 """

# * Importation de la classe qui représente les identificateurs
from tableid import IdentifierTable, IdentifierInfo, IdentifierType

from typing import Optional, List, Any

# * C'est dans le nom !
from prettytable import PrettyTable


class Compiler:
    """
    Cette classe représente un compilateur.

    Un compilateur est utilisé pour compiler un programme source en un programme exécutable.
    """

    def __init__(self):
        """
        Initialise le compilateur avec les structures de données nécessaires.
        """
        # * Structures
        self.instructions = []
        self.identifier_table = IdentifierTable()
        self.identifier_queue = []

        # * Counters
        # * Permet de compter les boucles et les alternatives imbriquées
        self.loop_counter = 0
        self.altern_counter = 0
        self.identifier_counter = 0
        self.param_counter = 0
        self.callable_counter = 0

        self.prog_princ: str = None

        # * Piles
        self.callable_stack = []
        self.scope_stack = []

        # * Variables
        self.scope = None
        self.current_address = 0

    # ===========================================
    # * Méthodes de gestion des instructions
    # ===========================================

    # * Méthodes pour ajouter des instructions basiques
    def mult(self):
        self.instructions.append(["mult"])

    def add(self):
        self.instructions.append(["add"])

    def affectation(self):
        self.instructions.append(["affectation"])

    def valeur_pile(self):
        self.instructions.append(["valeurPile"])

    def get(self):
        self.instructions.append(["get"])

    def reserver_bloc(self):
        self.instructions.append(["reserverBloc"])

    def retour_fonct(self):
        self.instructions.append(["retourFonct"])

    def debut_prog(self):
        self.instructions.append(["debutProg"])

    def fin_prog(self):
        self.instructions.append(["finProg"])

    def put(self):
        self.instructions.append(["put"])

    def moins(self):
        self.instructions.append(["moins"])

    def sous(self):
        self.instructions.append(["sous"])

    def div(self):
        self.instructions.append(["div"])

    def diff(self):
        self.instructions.append(["diff"])

    def inf(self):
        self.instructions.append(["inf"])

    def infeg(self):
        self.instructions.append(["infeg"])

    def sup(self):
        self.instructions.append(["sup"])

    def supeg(self):
        self.instructions.append(["supeg"])

    def ou(self):
        self.instructions.append(["ou"])

    def et(self):
        self.instructions.append(["et"])

    def erreur(self):
        self.instructions.append(["erreur"])

    def non(self):
        self.instructions.append(["non"])

    def egal(self):
        self.instructions.append(["egal"])

    def retour_constr(self):
        self.instructions.append(["retourConstr"])

    def retour_proc(self):
        self.instructions.append(["retourProc"])

    # * Méthodes avec paramètres
    def empiler(self, v: int, choix: int):
        if choix == 0:
            self.instructions.append(["empiler", v, "entier"])
        elif choix == 1:
            self.instructions.append(["empiler", v, "adresse"])
        else:
            self.instructions.append(["empiler", v, "boolean"])

    def empiler_ad(self, v: int):
        self.instructions.append(["empilerAd", v])

    def empiler_param(self, v: int):
        self.instructions.append(["empilerParam", v])

    def reserver(self, nb: int):
        self.instructions.append(["reserver", nb])

    def tze(self, a: int):
        self.instructions.append(["tze", a])

    def tra(self, a: int):
        self.instructions.append(["tra", a])

    def tra_stat(self, a: int, p: int):
        self.instructions.append(["traStat", a, p])

    # * Méthodes de manipulation d'instructions spécifiques
    def put_instruction(self, pos: int, instr: List):
        """
        Insère une instruction spécifique à une position donnée dans la liste des instructions.
        """
        self.instructions.insert(pos, instr)

    # ===========================================
    # * Méthodes de gestion des identificateurs
    # ===========================================

    def add_identifier(
        self,
        name: str,
        id_type: IdentifierType,
        scope: Optional[str],
        value: Optional[Any] = None,
        address: Optional[int] = None,
        is_in: bool = None,
        is_in_out: bool = None,
    ) -> None:
        """
        Ajoute un nouvel identificateur à la table avec des informations bien définies.

        Args:
            name (str): Le nom de l'identificateur.
            id_type (IdentifierType): Le type de l'identificateur.
            scope (str): La portée de l'identificateur.
            value (Optional[any]): La valeur initiale de l'identificateur, si applicable.
        """

        if id_type == IdentifierType.FUNCTION or id_type == IdentifierType.PROCEDURE:
            # * On décale la portée
            self.scope_stack.append(self.scope)
            # * On change la portée pour le nouvel identificateur
            if address is None:
                self.identifier_table.add_identifier(
                    name,
                    id_type,
                    self.scope,
                    value,
                    len(self.instructions) + 1,
                    is_in,
                    is_in_out,
                )
            else:
                self.identifier_table.add_identifier(
                    name, id_type, self.scope, value, address, is_in, is_in_out
                )
            self.scope = name

        else:
            self.identifier_table.add_identifier(
                name, id_type, self.scope, value, self.current_address, is_in, is_in_out
            )

    def leave_scope(self):
        """
        Restaure la portée précédente lorsqu'on quitte un contexte fonctionnel ou procédural.
        """
        if self.scope_stack:
            self.scope = self.scope_stack.pop()
        else:
            self.scope = None

    def reset_address(self):
        """
        Réinitialise l'adresse courante dans la gestion des identificateurs.
        """
        self.current_address = 0

    def increment_address(self):
        """
        Incrémente l'adresse courante utilisée pour les nouveaux identificateurs.
        """
        self.current_address += 1

    # ===========================================
    # * Méthodes d'affichage
    # ===========================================

    def print_identifier_table(self):
        """
        Retourne une chaîne de caractères contenant tous les identificateurs actuellement stockés dans la table des identificateurs, formatée en tableau.
        """
        table = PrettyTable()
        table.field_names = ["Name", "Type", "Scope", "Address"]
        for name, info in self.identifier_table.table.items():
            # value = info.get_value() if info.get_value() is not None else "None"
            address = info.get_address() if info.get_address() is not None else "None"
            table.add_row([name, info.get_type(), info.get_scope(), address])

        return table.get_string()

    def print_instructions(self):
        """
        Affiche les instructions sous forme de tableau avec les numéros de ligne.
        """
        table = PrettyTable()
        table.field_names = ["Line number", "Instruction"]
        for index, instruction in enumerate(self.instructions):
            match len(instruction):
                case 1:
                    table.add_row([index + 1, instruction[0] + "()"])
                case 2:
                    table.add_row(
                        [index + 1, instruction[0] + "(" + str(instruction[1]) + ")"]
                    )
                case 3:
                    if instruction[0] == "empiler":
                        table.add_row(
                            [
                                index + 1,
                                instruction[0] + "(" + str(instruction[1]) + ")",
                            ]
                        )
                    else:
                        table.add_row(
                            [
                                index + 1,
                                instruction[0]
                                + "("
                                + str(instruction[1])
                                + ","
                                + str(instruction[2])
                                + ")",
                            ]
                        )
        return table.get_string()

    def print_test(self):
        """
        Affiche une représentation textuelle des instructions pour des tests.
        """
        for instruction in self.instructions:
            match len(instruction):
                case 0:
                    pass
                case 1:
                    print(instruction[0] + "()")
                case 2:
                    print(instruction[0] + "(" + str(instruction[1]) + ")")
                case 3:
                    if instruction[0] == "empiler":
                        print(instruction[0] + "(" + str(instruction[1]) + ")")
                    else:
                        print(
                            instruction[0]
                            + "("
                            + str(instruction[1])
                            + ","
                            + str(instruction[2])
                            + ")"
                        )
