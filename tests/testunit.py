import unittest
import subprocess
import glob
import os
from colorama import Fore, Style


class TestUnit(unittest.TestCase):
    def run_compiler(self, file_path):
        result = subprocess.run(
            ["python3", "./src/anasyn.py", file_path, "--test-unit"],
            capture_output=True,
            text=True,
        )
        if result.returncode != 0:
            return None, result.stderr
        output = [
            line.strip().split() for line in result.stdout.splitlines() if line.strip()
        ]
        return output, None

    def compare_and_highlight(self, actual, expected):
        """Compare outputs and highlight differences."""
        differences = []
        max_length = max(len(actual), len(expected))
        for i in range(max_length):
            actual_line = actual[i] if i < len(actual) else ["---"]
            expected_line = expected[i] if i < len(expected) else ["---"]
            if actual_line == expected_line:
                differences.append(Fore.GREEN + " ".join(actual_line) + Style.RESET_ALL)
            else:
                differences.append(
                    Fore.RED + " ".join(actual_line) + " (Actual)" + Style.RESET_ALL
                )
                differences.append(
                    Fore.YELLOW
                    + " ".join(expected_line)
                    + " (Expected)"
                    + Style.RESET_ALL
                )
        return differences

    def test_iterate_files(self):
        test_files = (
            glob.glob("./tests/local/correct1.nno")
            + glob.glob("./tests/nna/correct*.nno")
            + glob.glob("./tests/nnp/*.nno")
        )
        for file_path in test_files:
            expected_file = file_path + ".expected"
            if not os.path.exists(expected_file):
                print(
                    Fore.YELLOW
                    + f"Test skipped for {file_path}: .expected file missing."
                    + Style.RESET_ALL
                )
                continue

            with self.subTest(file_path=file_path):
                output, error = self.run_compiler(file_path)
                if error:
                    print(
                        Fore.RED
                        + f"Compiler error on {file_path}: {error}"
                        + Style.RESET_ALL
                    )
                    self.fail("Compiler error")

                with open(expected_file, "r") as f:
                    expected_output = [
                        line.strip().split() for line in f.readlines() if line.strip()
                    ]

                differences = self.compare_and_highlight(output, expected_output)
                if not any(Fore.RED in diff for diff in differences):
                    print(
                        Fore.GREEN
                        + f"Success: Output matches expectations for {file_path} ✅"
                        + Style.RESET_ALL
                    )
                else:
                    print(
                        Fore.RED
                        + f"Failure: Output mismatch for {file_path}. ❌"
                        + Style.RESET_ALL
                    )
                    for diff in differences:
                        print(diff)
                    self.fail("Result mismatch")


if __name__ == "__main__":
    unittest.main()
