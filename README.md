# Projet de théorie des langages et compilation

## Objectif

Ce projet consiste en la réalisation d'un compilateur pour le langage NILNOVI PROCÉDURAL. Le compilateur est construit à partir d'un analyseur lexical et d'un analyseur syntaxique, avec pour objectif final la génération de code objet exécutable.

## Développement et gestion du projet

Le projet a été développé en utilisant le langage Python.

### Prérequis

- Python 3.6 ou version ultérieure

### Dépendances
```bash
    pip install -r requirements.txt
```

- pip3
- colorama
- black

## Taches à faire

- [X] Table des identificateurs (nom, type, adresse des identificateurs)
- [X] Génération du code objet. (NNA/NNP)
- [X] Points de générations identifiés
- [X] Insérer les appels aux points de génération (NNA/NNP)
- [X] Machine virtuelle
- [ ] Vérifications sémantiques

## Tests unitaires
```bash
python3 tests/testunit.py
```

## Exécution
```bash
python3 src/anasyn.py tests/.../fichier.nno
```

### Options de ligne de commande

--execution : Execute le code à l'aide de la machine virtuelle.
--show-pile : Permet de consulter l'état de la pile (nécessite le flag --execution)
--show-instruction : Liste les instructions en code objet
--ident-table : Affiche la table des identificateurs

## Superviseur

- Damien Lolive

## Équipe de développement

- Loueï BEKHEDDA
- Mathias PEUCH
- Mathias PENAVAIRE
- Barbara ABID-BRAGA

## License

MIT License

Copyright (c) 2021

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
